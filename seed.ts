import { createConnection } from 'typeorm';
import { RoomEntity } from './src/entities/room.entity';

const seedDatabase = async () => {
    try {
        const connection = await createConnection();
        const roomRepository = connection.getRepository(RoomEntity);

        await roomRepository.clear();

        // Insert the desired data
        const rooms = [
            { id:1 , name: 'Room 1' },
            { id:2 , name: 'Room 2' },
            { id:3 , name: 'Room 3' },
        ];

        await roomRepository.insert(rooms);

        console.log('Data inserted successfully');
        await connection.close();
    } catch (error) {
        console.error('Error seeding the database:', error);
    }
};

seedDatabase();