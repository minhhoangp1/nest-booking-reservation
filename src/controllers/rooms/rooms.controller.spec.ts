import { Test, TestingModule } from '@nestjs/testing';
import { RoomsController } from './rooms.controller';
import { RoomsService } from '../../services/rooms/rooms.service';
import { RoomEntity } from '../../entities/room.entity';
import { RoomEntityRepository } from '../../repositories/room.repository';
import { getRepositoryToken } from '@nestjs/typeorm';

describe('RoomsController', () => {
  let controller: RoomsController;

  const mockRooms = [
    { id: 1, name: 'Room 1' },
    { id: 2, name: 'Room 2' },
  ];

  const mockRoomEntityRepository = () => ({
    find: jest.fn().mockResolvedValue(mockRooms),
  });

  const mockRoom = { id: 1, name: 'Room 1' };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RoomsController],
      providers: [
        RoomsService,
        {
          provide: getRepositoryToken(RoomEntity),
          useFactory: mockRoomEntityRepository,
        },
      ],
    }).compile();

    controller = module.get<RoomsController>(RoomsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  // it('should return an array of rooms when calling getAllRooms', async () => {
  //   const result = await controller.getAllRooms();
  //   expect(result).toEqual(expect.arrayContaining(mockRooms));
  // });

  // it('should return a room when calling getRoomById', async () => {
  //   const roomId = '1'; // Replace with an existing room ID from your mock data
  //   const result = await controller.getRoomById(roomId);
  //   expect(result).toEqual(mockRoom);
  // });

  // // it('should create a new room when calling createRoom', async () => {
  // //   const roomDto = { name: 'New Room' }; // Replace with the desired room data
  // //   const result = await controller.createRoom(roomDto);
  // //   expect(result).toEqual('Room created successfully');
  // // });

  // it('should update a room when calling updateRoom', async () => {
  //   const roomId = '1'; // Replace with an existing room ID from your mock data
  //   const roomDto = { name: 'Updated Room' }; // Replace with the desired room data
  //   const result = await controller.updateRoom(roomId, roomDto);
  //   expect(result).toEqual('Room updated successfully');
  // });

  // it('should delete a room when calling deleteRoom', async () => {
  //   const roomId = '1'; // Replace with an existing room ID from your mock data
  //   const result = await controller.deleteRoom(roomId);
  //   expect(result).toEqual('Room deleted successfully');
  // });

  // it('should create a new room when calling createRoom', () => {
  //   const roomDto = { name: 'New Room' }; // Replace with the desired room data
  //   const result = controller.createRoom(roomDto);
  //   expect(result).toEqual('Room created successfully');
  // });
});