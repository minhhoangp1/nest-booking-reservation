import { Controller, Get, Post, Put, Delete, Param, Body } from '@nestjs/common';
import { RoomsService } from '../../services/rooms/rooms.service';

@Controller('rooms')
export class RoomsController {
    constructor(private readonly roomsService: RoomsService) { }

    @Get()
    async getAllRooms() {
      return await this.roomsService.getAllRooms();
    }

    @Get(':id')
    async getRoomById(@Param('id') id: string) {
        return await this.roomsService.getRoomById(+id);
    }

    @Put(':id')
    async updateRoom(@Param('id') id: string, @Body() roomDto: any) {
        return await this.roomsService.updateRoom(+id, roomDto);
    }

    @Delete(':id')
    async deleteRoom(@Param('id') id: string) {
        return await this.roomsService.deleteRoom(+id);
    }

    // @Delete(':id')
    // deleteRoom(@Param('id') id: string) {
    //     // Logic to delete a room by ID
    // }

    // @Post()
    // createRoom(@Body() roomDto: CreateRoomDto) {
    //     // Logic to create a new room
    // }

    // @Put(':id')
    // updateRoom(@Param('id') id: string, @Body() roomDto: UpdateRoomDto) {
    //     // Logic to update a room by ID
    // }
}

