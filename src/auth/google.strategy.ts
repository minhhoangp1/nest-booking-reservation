// google.strategy.ts

import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, VerifyCallback } from 'passport-google-oauth20';
import { AuthService } from './auth.service';
import { UserRole } from 'src/user/roles.enum';

import { JwtService } from '@nestjs/jwt';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
  constructor(
    private readonly authService: AuthService,
    private readonly jwtService: JwtService,
  ) {
    super({
      clientID: '205795537126-aftqm0fh1i822qhopnb7p7pu8m352u60.apps.googleusercontent.com',
      clientSecret: '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',
      callbackURL: 'http://localhost:3000/auth/google/callback',
      passReqToCallback: true,
      scope: ['profile', 'email'],
    });

  }

  async validate(
    req: any,
    accessToken: string,
    refreshToken: string,
    profile: any,
    done: VerifyCallback,
  ): Promise<any> {
    try {
      const user = {
        email: profile.emails[0].value,
        displayName: profile.displayName,
        googleId: profile.id,
        role: profile.emails[0].value === 'cpthoang@gmail.com' ? UserRole.ADMIN : UserRole.USER,
      };

      const payload = {
        email: user.email,
        sub: user.googleId,
        role: user.role,
      };
      const jwt = this.jwtService.sign(payload);

      // Attach the user object and the JWT to the request object
      req.user = user;
      req.user.jwt = jwt;

      console.log('Validating user:', user);

      return done(null, user);
    } catch (error) {
      return done(error, false);
    }
  }
}
