// auth.controller.ts

import { Controller, Get, Req, Res, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';

@Controller('auth')
export class AuthController {

  @Get('google')
  @UseGuards(AuthGuard('google'))
  async googleAuth() { }

  @Get('google/callback')
  @UseGuards(AuthGuard('google'))
  async googleAuthCallback(@Req() req: any, @Res({ passthrough: true }) res: Response) {
    if (req.user) {
      const userInfo = {
        displayName: req.user.displayName,
        email: req.user.email,
      };


      // Retrieve the token from the user object
      const token = req.user.jwt;
      console.log('controller token: ' + JSON.stringify(token));

      // Return the token to the client
      res.cookie('token', token); // Set the token in a cookie or include it in the response body

      return `Login success! User: ${JSON.stringify(userInfo)}`;
    } else {
      return 'Login failed!';
    }
  }

  @Get('logout')
  logout(@Req() req, @Res() res: Response) {
    if (req.cookies && req.cookies.token) {
      console.log('Existing token cookie:', req.cookies.token);
      res.clearCookie('token'); // Clear the token from the cookie
      req.cookies.token = ''; // Clear the token from the cookie
      
      console.log('Clear cookie -> Existing token cookie:', req.cookies.token);
    } else {
      console.log('Token cookie not found.');
    } 
    
    console.log('User successfully logged out');
    // Redirect or perform any other actions after successful logout
    res.redirect('/'); // Redirect to the home page or login page
  }
}
