import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { GoogleStrategy } from './google.strategy';
import { JwtModule } from '@nestjs/jwt';
import { JWTModule } from 'src/user/jwt.module';
import { APP_GUARD } from '@nestjs/core';
import { JwtStrategy } from 'src/user/jwt.strategy';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'google', session: true }),
    JWTModule
  ],
  providers: [
    AuthService,
    GoogleStrategy,
    JwtStrategy 
  ],
  exports: [
    AuthService,
    PassportModule
  ],
})
export class AuthModule {}
