// // auth.controller.spec.ts
// import { Test, TestingModule } from '@nestjs/testing';
// import { AuthController } from './auth.controller';
// import { AuthService } from './auth.service';
// import { AuthGuard } from '@nestjs/passport';

// jest.mock('@nestjs/passport', () => ({
//   AuthGuard: jest.fn().mockImplementation(() => ({
//     canActivate: jest.fn().mockImplementation(() => true),
//   })),
// }));

// describe('Auth Controller', () => {
//   let controller: AuthController;

//   beforeEach(async () => {
//     const module: TestingModule = await Test.createTestingModule({
//       controllers: [AuthController],
//       providers: [
//         {
//           provide: AuthService,
//           useValue: {},
//         },
//       ],
//     })
//       .overrideGuard(AuthGuard('google'))
//       .useValue({ canActivate: () => true })
//       .compile();

//     controller = module.get<AuthController>(AuthController);
//   });

//   it('should be defined', () => {
//     expect(controller).toBeDefined();
//   });

//   describe('googleAuthCallback', () => {
//     it('should return user info if user is logged in', () => {
//       const req = { user: { displayName: 'John Doe', email: 'john.doe@example.com' } };
//       expect(controller.googleAuthCallback(req)).toEqual(
//         `Login success! User: ${JSON.stringify({ displayName: 'John Doe', email: 'john.doe@example.com' })}`,
//       );
//     });

//     it('should return failed login if user is not logged in', () => {
//       const req = { user: null };
//       expect(controller.googleAuthCallback(req)).toEqual('Login failed!');
//     });
//   });

//   describe('logout', () => {
//     it('should handle successful logout', () => {
//       const req = { logout: jest.fn().mockImplementationOnce((cb) => cb()) };
//       const res = { redirect: jest.fn() };
//       controller.logout(req, res as any);
//       expect(req.logout).toHaveBeenCalled();
//       expect(res.redirect).toHaveBeenCalledWith('/');
//     });

//     it('should handle logout failure', () => {
//       const req = { logout: jest.fn().mockImplementationOnce((cb) => cb(new Error('Logout error'))) };
//       const res = { redirect: jest.fn() };
//       expect(() => controller.logout(req, res as any)).toThrowError('Logout error');
//     });
//   });
// });
