//app.module.ts

import { Module, NestModule, MiddlewareConsumer  } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RoomsController } from './controllers/rooms/rooms.controller';
import { RoomsService } from './services/rooms/rooms.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoomEntity } from './entities/room.entity';
import { RoomEntityRepository } from './repositories/room.repository';

import { AuthModule } from './auth/auth.module';
import { AuthController } from './auth/auth.controller';

import { AdminController } from './user/admin.controller';

import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './user/jwt.strategy';
import { PassportModule } from '@nestjs/passport';

// import LoggerMiddleware
import { LoggerMiddleware } from './logger.middleware';
import { GoogleStrategy } from './auth/google.strategy';
import { JWTModule } from './user/jwt.module';
import { APP_GUARD } from '@nestjs/core';
import { RolesGuard } from './user/roles.guard';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: "./data/database.sqlite",
      entities: [RoomEntity],
      // synchronize: true, // Auto-create database tables (for development only)
    }),
    TypeOrmModule.forFeature([RoomEntity]),

    AuthModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),

    JWTModule
  ],
  controllers: [AppController, RoomsController, AuthController, AdminController],
  providers: [AppService, RoomsService, RoomEntityRepository, JwtStrategy, RolesGuard
],
})

export class AppModule implements NestModule { // added "implements NestModule"
  configure(consumer: MiddlewareConsumer) { // added "configure" method
    consumer
      .apply(LoggerMiddleware)
      .forRoutes('*');
  }
}
