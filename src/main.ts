//main.ts

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as passport from 'passport';
import * as cookieParser from 'cookie-parser'; // Import cookie-parser

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(cookieParser()); // Add cookie-parser middleware
  app.use(passport.initialize());

  await app.listen(3000);
}
bootstrap();
