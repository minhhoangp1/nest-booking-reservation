// admin.controller.ts
import { Controller, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from './roles.decorator';
import { RolesGuard } from './roles.guard';
import { UserRole } from './roles.enum';
import { Request } from 'express';
import { Req } from '@nestjs/common';


@Controller('admin')
@UseGuards(AuthGuard('jwt'))
export class AdminController {
  @UseGuards(RolesGuard)
  @Get()
  @Roles(UserRole.ADMIN)
  getAdminPage() {
    const data = 'getAdminPage';
    return data;
  }
}
