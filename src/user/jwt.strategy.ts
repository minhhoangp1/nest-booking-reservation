//jwt.strategy.ts

import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { UserRole } from './roles.enum';
import { Reflector } from '@nestjs/core';

export interface JwtPayload {
    email: string;
    sub: string; // Google ID
    role: UserRole; // User role
}


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor() {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: '39rfb0af3p98f2q3rbq34pf9834rfv9834fbv9w834tv98a34t',
        });
    }

    async validate(payload: JwtPayload) {
        console.log('Validating JWT payload:', payload);

        return { googleId: payload.sub, email: payload.email, role: payload.role };
    }
}