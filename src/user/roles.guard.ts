// roles.guard.ts
import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { UserRole } from './roles.enum';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) { }

  canActivate(context: ExecutionContext): boolean {
    const requiredRoles = this.reflector.getAllAndOverride<UserRole[]>('roles', [
      context.getHandler(),
      context.getClass(),
    ]);

    const request = context.switchToHttp().getRequest();
    const user = request.user;

    console.log('Required roles:', requiredRoles);
    if (!requiredRoles) {
      return true;
    }

    console.log('Current request rawHeaders:', request.rawHeaders);
    console.log('Current user:', user);

    // if (!user) {
    //   throw new UnauthorizedException('User not found in request');
    // }

    return requiredRoles.some((role) => user.role?.includes(role));
  }
}



