import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { JwtModule } from "@nestjs/jwt";

@Module({
    imports: [
        {
            ...JwtModule.registerAsync({
                imports: [ConfigModule],
                useFactory: async (configService: ConfigService) => ({
                    secret: '39rfb0af3p98f2q3rbq34pf9834rfv9834fbv9w834tv98a34t',
                    signOptions: {
                        expiresIn: 3600,
                    },
                }),
                inject: [ConfigService],
            }),
            global: true
        }
    ],
    exports: [JwtModule]
})

export class JWTModule { }