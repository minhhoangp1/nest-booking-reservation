import { Test, TestingModule } from '@nestjs/testing';
import { RoomsService } from './rooms.service';
import { RoomEntity } from '../../entities/room.entity';
import { RoomEntityRepository } from '../../repositories/room.repository';
import { getRepositoryToken } from '@nestjs/typeorm';

describe('RoomsService', () => {
  let service: RoomsService;
  let mockRepository; // to remove the need for a real database or repository.

  beforeEach(async () => {
    mockRepository = {
      find: jest.fn().mockResolvedValue([
        { id: 1, name: 'Room 1' },
        { id: 2, name: 'Room 2' },
        { id: 3, name: 'Room 3' },
      ]),
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RoomsService,
        {
          provide: getRepositoryToken(RoomEntity),
          useValue: mockRepository,
        },
      ],
    }).compile();

    service = module.get<RoomsService>(RoomsService);
  });

  it('should return an array of rooms when calling getAllRooms', async () => {
    const result = await service.getAllRooms();
    expect(result).toEqual([
      { id: 1, name: 'Room 1' },
      { id: 2, name: 'Room 2' },
      { id: 3, name: 'Room 3' },
    ]);
    expect(mockRepository.find).toHaveBeenCalled();
  });

  // Add more test cases for other methods
});