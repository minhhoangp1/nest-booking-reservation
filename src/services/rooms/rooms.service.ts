import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RoomEntity } from '../../entities/room.entity';
import { RoomEntityRepository } from '../../repositories/room.repository';

@Injectable()
export class RoomsService {

    constructor(
        @InjectRepository(RoomEntity)
        private readonly roomRepository: Repository<RoomEntity>,
    ) { }
    

    async getAllRooms() {
        console.log('Loading');
        const rooms = await this.roomRepository.find();
        console.log('Service getAllRooms', rooms);
        return rooms;
    }

    async getRoomById(id: number) {
        return await this.roomRepository.findOne({ where: { id } });
    }

    async updateRoom(id: number, roomDto: any) {
        const room = await this.roomRepository.findOne({ where: { id } });

        if (!room) {
            throw new Error('Room not found');
        }

        room.name = roomDto.name;

        await this.roomRepository.save(room);

        return 'Room updated successfully';
    }

    async deleteRoom(id: number) {
        const room = await this.roomRepository.findOne({where: {id} });

        if (!room) {
            throw new Error('Room not found');
        }

        await this.roomRepository.remove(room);

        return 'Room deleted successfully';
    }
}
